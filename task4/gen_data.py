import numpy as np
import random

def generate_random_russian_string():
    length = [int(x) for x in np.random.uniform(0, 1, 1) * 20][0]
    russian_alphabet = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя '
    return ''.join(random.choice(russian_alphabet) for _ in range(length))

def generate_text_data(num_samples):
    data = []
    for cnt in range(num_samples):
        sample = {
            "id": cnt,
            "text_emb": generate_random_russian_string(),
            "name": generate_random_russian_string(),
            "category": generate_random_russian_string(),
            "address": generate_random_russian_string(),
            "length": np.random.uniform(-1, 1, 1)[0],
            "width": np.random.uniform(-1, 1, 1)[0]
        }
        data.append(sample)
    return data