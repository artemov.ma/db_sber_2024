Предисловие: Для работы с MongoDB я буду использовать их GUI MongoDB Compass. Для этого установлю .msi файл с их сайта и установлю его. После запуска подключусь к localhost:27017. Экран программы будет выглядеть так

![alt text](pic/1_Open_Window.png)

Создадим базу данных и коллекцию внутри нее

![alt text](pic/2_Create_Base_and_Collection.png)

В качестве дата сета буду использовать Wine Quality Dataset для белых вин. Загрузим его в базу данных

![alt text](pic/3_Import_Data.png)

Посмотрим как выглядят данные

![alt text](pic/4_Import_Data_Result.png)

Попробуем добавить даные в таблицу

![alt text](pic/5_Add_Data.png)

Посмотрим на добавленные данные через фильтр

![alt text](pic/6_Add_Data_Result.png)

Обновим данные с помощью внутренного интерфейса. Воспользуемся инкрементированием, умножением и set'ом

![alt text](pic/7_Update_Data.png)

Посмотрим как выглядят обновленные данные

![alt text](pic/8_Update_Data_Result.png)

Сформируем сложный запрос для выдачи и посмотрим сколько элементов ему соответсвует 

![alt text](pic/9_Read_with_Filter.png)

Теперь посмотрим сколько вин удовлетворяет нужным требованиям по качеству

![alt text](pic/10_Read_with_Filter.png)

Всего 8!

Теперь удалим данные по индексу

![alt text](pic/11_Delete.png)

Объекта и правда нет в таблице. Find его не находит

![alt text](pic/12_Delete_Result.png)

Создадим запрос для проверки работы с индексом и без

![alt text](pic/14_Query_for_checking_speed.png)

Посмотрим сколько работал запрос без введения индекса

![alt text](pic/15_Query_without_index.png)

Время работы составила 4 мс

Теперь создадим индекс на alcohol

![alt text](pic/13_Create_Index.png)

Взглянем на время

![alt text](pic/16_Query_with_index.png)

А теперь время работы 0 мс!



